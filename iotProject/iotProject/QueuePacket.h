//
// Created by Joshua Taylor on 4/9/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface QueuePacket : NSObject

@property (strong, nonatomic) NSString *queuePosition;
@property (strong, nonatomic) NSString *userId;

- (instancetype)initQueueWithUserPickupPoint:(NSString *) pickupPoint withDestination:(NSString *) destination;

- (NSDictionary *) convertPacketToDictionary;

@end