//
// Created by Joshua Taylor on 4/1/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Location : NSObject

@property NSString *xValue;
@property NSString *yValue;
@property NSString *xPreviousPostion;
@property NSString *yPreviousPostion;

- (instancetype) initWithCoordinate: (NSDictionary *)dictionary;

@end