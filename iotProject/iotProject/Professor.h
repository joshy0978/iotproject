//
// Created by Joshua Taylor on 4/1/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Firebase.h"

@class Location;


@interface Professor : NSObject

@property (strong, atomic) NSString *firstName;
@property (strong, atomic) NSString *lastName;
@property (strong, atomic) NSString *roomNumber;
@property (strong, atomic) Location *roomCoordinate;
@property (strong, atomic) NSString *imageLink;

- (id) initWithSnapShot:(FIRDataSnapshot *) snapShot;

- (NSString *) getProfessorFullName;

@end