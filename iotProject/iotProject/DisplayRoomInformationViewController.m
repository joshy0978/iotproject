//
//  DisplayRoomInformationViewController.m
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import "DisplayRoomInformationViewController.h"
#import "Professor.h"
#import "RobotStatusViewController.h"
#import "QueuePacket.h"

@interface DisplayRoomInformationViewController ()

@property (strong, nonatomic) NSArray *pickupLocations;
@property (strong, nonatomic) NSString *pickupLocation;

@end

@implementation DisplayRoomInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    self.pickupLocations = @[@"Point A", @"Point B", @"Point C", @"Point D"];

    self.pickUpPointPicker.dataSource = self;
    self.pickUpPointPicker.delegate = self;


    self.nameLable.alpha = 0;
    self.roomNumber.alpha = 0;
    self.goButton.alpha = 0;
    self.imageView.alpha = 0;
    self.pickUpPointPicker.alpha = 0;
    self.pickupLabel.alpha = 0;

    self.nameLable.text = [self.professor getProfessorFullName];
    self.roomNumber.text = self.professor.roomNumber;

    self.imageView.image = [UIImage imageNamed:self.professor.imageLink];


    [UIView animateWithDuration:0.5 animations:^{
        self.nameLable.alpha = 1;
    } completion:^(BOOL finished) {


        [UIView animateWithDuration:0.5
                         animations:^{
                             self.imageView.alpha = 1;
                         } completion:^(BOOL finished) {

                    [UIView animateWithDuration:0.5 animations:^{
                        self.roomNumber.alpha = 1;
                    } completion:^(BOOL finished) {

                        [UIView animateWithDuration:0.5 animations:^{
                            self.goButton.alpha = 1;
                        } completion:^(BOOL finished) {


                            [UIView animateWithDuration:0.5 animations:^{
                                self.pickUpPointPicker.alpha = 1;
                                self.pickupLabel.alpha = 1;
                            }];
                        }];



                    }];

                }];


    }];


    self.pickupLocation = [self.pickupLocations objectAtIndex:[self.pickUpPointPicker selectedRowInComponent:0] ];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)getRobot:(id)sender {

    QueuePacket * queue = [[QueuePacket alloc] initQueueWithUserPickupPoint:self.pickupLocation withDestination:self.professor.roomNumber];
    RobotStatusViewController *robotStatusViewController = [[RobotStatusViewController alloc] initWithNibName:@"RobotStatusViewController" bundle:nil];
    robotStatusViewController.queueItem = queue;
    [UIView animateWithDuration:1.0
                     animations:^{
                         self.goButton.transform = CGAffineTransformMakeRotation((CGFloat)(3.14/2));

                     }
                     completion:^(BOOL finished) {

                         CGPoint _center = self.goButton.center;
                         _center.x =  (self.goButton.center.x + self.view.bounds.size.width);

                         [UIView animateWithDuration:2.0
                                          animations:^{
                                              self.goButton.center = _center;
                                          }
                                          completion:^(BOOL finished) {


                                              //[self.navigationController pushViewController:robotStatusViewController animated:YES];
                                              [self presentViewController:robotStatusViewController animated:YES completion:^{

                                                  self.goButton.transform = CGAffineTransformMakeRotation((CGFloat)(3.14*2));
                                                  CGPoint originalPostiong = self.goButton.center;
                                                  originalPostiong.x = (self.goButton.center.x - self.view.bounds.size.width);
                                                  self.goButton.center = originalPostiong;

                                              }];
                                          }];



                     }];




}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.pickupLocations count];
}


- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.pickupLocations[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    self.pickupLocation = self.pickupLocations[row];
}



@end
