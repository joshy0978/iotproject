//
// Created by Joshua Taylor on 4/1/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import "Location.h"


@implementation Location {

}


- (id)initWithCoordinate:(NSDictionary *)dictionary {

    if ((self = [super init])) {


        self.xValue = dictionary[@"x"];
        self.yValue = dictionary[@"y"];

    }
    return self;
}



@end