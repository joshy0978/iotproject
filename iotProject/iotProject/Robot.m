//
// Created by Joshua Taylor on 4/2/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import "Robot.h"
#import "FIRDataSnapshot.h"
#import "QueueHandler.h"


@implementation Robot {

}

-(instancetype) initRobotWithSnapShot:(FIRDataSnapshot *) snapShot {

    if ((self = [super init])) {

        self.status = [NSNumber numberWithBool:[snapShot.value[@"status"] integerValue]];
        self.currentPosition = [[Location alloc] initWithCoordinate:snapShot.value[@"currentPosition"]];
        self.goingTo = snapShot.value[@"goingTo"];

    }

    return self;

}

- (NSString *) getRobotStatus{

    if (![self.status isEqualToNumber:@(0)]) {

        if ([QueueHandler sharedInstance].isCurrentRequest) {
            return @"You're Now Being Helped";
        } else {
            return @"Busy";
        }
    }

    return @"Open";
}

@end