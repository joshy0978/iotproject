//
//  ProfessorTableViewController.m
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import "ProfessorTableViewController.h"
#import "FBDatabaseApi.h"
#import "Professor.h"
#import "DisplayRoomInformationViewController.h"

@interface ProfessorTableViewController ()


@property (strong, atomic) NSArray *dataSource;

@end

@implementation ProfessorTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.dataSource = [NSArray new];
    self.title = @"Where To";
    

    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[FBDatabaseApi sharedInstance] getProfessorDataWithBlock:^(NSArray *data) {
        self.dataSource = data;
        
        [self.tableView reloadData];

    }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    // Set the data for this cell:
    Professor *professor = [self.dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", professor.firstName, professor.lastName];


    // set the accessory view:
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    DisplayRoomInformationViewController *displayRoomInformationViewController = [[DisplayRoomInformationViewController alloc] initWithNibName:@"DisplayRoomInformationViewController" bundle:nil];
    // Pass the selected object to the new view controller.
    displayRoomInformationViewController.professor = [self.dataSource objectAtIndex:indexPath.row];
    // Push the view controller.
    [self.navigationController pushViewController:displayRoomInformationViewController animated:YES];
}




@end
