//
//  RobotStatusViewController.h
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QueuePacket.h"

@interface RobotStatusViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UILabel *goToLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelRequestButton;
@property (weak, nonatomic) IBOutlet UIButton *closeWindowButton;

@property (strong, nonatomic) QueuePacket *queueItem;

@end
