//
// Created by Joshua Taylor on 4/2/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>


@class FIRDataSnapshot;

#import "Location.h"


@interface Robot : NSObject

@property (strong, atomic) NSNumber *status;
@property (strong, atomic) Location *currentPosition;
@property (strong, atomic) NSString *goingTo;


-(instancetype) initRobotWithSnapShot:(FIRDataSnapshot *) snapShot;
- (NSString *) getRobotStatus;

@end