//
// Created by Joshua Taylor on 4/9/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import "QueuePacket.h"


@interface QueuePacket()

@property (strong, setter=isCancelled:) NSString *cancelled;
@property (strong, nonatomic) NSString *destination;
@property (strong, nonatomic) NSString *pickupPoint;



@end

@implementation QueuePacket {

}

- (instancetype)initQueueWithUserPickupPoint:(NSString *) pickupPoint withDestination:(NSString *) destination  {

    if (self = [super init]) {

        self.pickupPoint = [self convertPickupPointToSingleLetter:pickupPoint];
        self.destination = destination;
        self.userId = [[NSProcessInfo processInfo] globallyUniqueString];
        self.cancelled = @"NO";
        self.queuePosition = nil;


    }


    return self;
}

- (NSDictionary *)convertPacketToDictionary {

    NSDictionary * dictionary = @{
            @"cancelled" : self.cancelled,
            @"destination" : self.destination,
            @"pickupPoint" : self.pickupPoint,
            @"userId" : self.userId

    };
    return dictionary;
}

- (NSString *) convertPickupPointToSingleLetter:(NSString *)pickupPoint {


    //"Point A", @"Point B", @"Point C", @"Point D"
    if ([pickupPoint isEqualToString:@"Point A"]) {
        return @"A";
    } else if ([pickupPoint isEqualToString:@"Point B"]) {
        return @"B";
    } else if ([pickupPoint isEqualToString:@"Point C"]) {
        return @"C";
    } else if ([pickupPoint isEqualToString:@"Point D"]) {
        return @"D";
    }

    return pickupPoint;
}


@end
