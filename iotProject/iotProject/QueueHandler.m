//
//  QueueHandler.m
//  iotProject
//
//  Created by Joshua Taylor on 4/9/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import "QueueHandler.h"
#import "QueuePacket.h"
#import "FBDatabaseApi.h"

@interface QueueHandler ()


@property (strong, nonatomic) NSString *queuePosition;
@property (strong, nonatomic) QueuePacket * queuePacket;


@end


@implementation QueueHandler

+ (QueueHandler *)sharedInstance {

    static QueueHandler * sharedInstance = nil;
    static dispatch_once_t dispatch_once1;

    dispatch_once(&dispatch_once1, ^{

        sharedInstance = [[QueueHandler alloc] initPrivate];
    });


    return sharedInstance;
}


- (instancetype) initPrivate {
    if (self = [super init]) {
        self.queuePacket = nil;
        self.queuePosition = nil;
        self.isRequestCompleted = [NSNumber numberWithBool:NO];
    }


    return self;
}

-  (void) sendRequest:(QueuePacket *)questPacket {

    self.queuePacket = questPacket;

    [[FBDatabaseApi sharedInstance] pushQueuePacket:questPacket withBlock:^(NSString *queuePosition) {
        self.queuePosition = queuePosition;
    }];

}

- (void)cancelRequest {

    [[FBDatabaseApi sharedInstance] cancelRequest:self.queuePosition];

}

- (void) updateFrontQueuePosition {

    [[FBDatabaseApi sharedInstance] getFrontOfQueueValue:^(NSString *queueSize) {
        self.frontQueuePosition = queueSize;
    }];

}

- (BOOL)isCurrentRequest {

    [self updateFrontQueuePosition];

    if ([self.frontQueuePosition isEqualToString:self.queuePosition]) {

        self.isRequestCompleted = [NSNumber numberWithBool:YES];
        return YES;
    }

    return NO;
}


@end
