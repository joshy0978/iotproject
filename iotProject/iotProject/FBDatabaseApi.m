//
//  FBDatabaseApi.m
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import "FBDatabaseApi.h"
#import "Firebase.h"
#import "Professor.h"
#import "Robot.h"
#import "QueuePacket.h"

@interface FBDatabaseApi()

@property (strong, atomic) FIRDatabaseReference *ref;


@end

@implementation FBDatabaseApi

+ (FBDatabaseApi*)sharedInstance
{
    static dispatch_once_t predicate = 0;
    static id sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] initPrivate];
    });
    return sharedObject;
}

- (instancetype)initPrivate {
    if (self = [super init]) {
        self.ref = [[FIRDatabase database] reference];
    }
    return self;
}


- (void)getProfessorDataWithBlock:(void (^)(NSArray * data))callback {

    NSMutableArray * professors = [NSMutableArray new];

    [[self.ref child:@"SecondFloor"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {

        for (FIRDataSnapshot *child in [snapshot children]) {
            Professor *prof = [[Professor alloc] initWithSnapShot:child];
            
            [professors addObject:prof];
        }
        
        for(Professor *prof in professors) {
            NSLog(@"Professor Name: %@", prof.firstName);
        }
        
        NSArray * copy = [professors copy];
        
        
        callback(copy);
        
        [professors removeAllObjects];
    }];

}

- (void)getRobotStatusWithBlock:(void (^)(Robot *robot))callback {

    [[self.ref child:@"irobot"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {


        Robot * robot = [[Robot alloc] initRobotWithSnapShot:snapshot];

        callback(robot);
    }];

}

- (void)pushQueuePacket:(QueuePacket *)queuePacket withBlock:(void (^)(NSString *queuePosition))callback {



    [[self.ref child:@"irobot"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {

        
        NSString *tailPosition;

        NSDictionary *dictionary = snapshot.value[@"queue"];
        tailPosition = dictionary[@"backOfQueue"];
        NSString *newTailPosition = [@([tailPosition integerValue] + 1) stringValue];

        NSDictionary *packet = [queuePacket convertPacketToDictionary];
        NSDictionary *childUpdates = @{

                [@"/irobot/queue/" stringByAppendingString:tailPosition]: packet,
                 @"/irobot/queue/backOfQueue/" : newTailPosition

        };


        [self.ref updateChildValues:childUpdates];

        callback(tailPosition);

    }];

}

- (void)cancelRequest:(NSString *) queuePosition{


    [[[[self.ref child:@"irobot"] child:@"queue"] child:[queuePosition stringByAppendingString:@"/cancelled"]] setValue:@"YES"];

}

- (void)getFrontOfQueueValue:(void (^)(NSString * frontOfQueue)) callback {

    [[self.ref child:@"irobot/queue"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {


        NSString * frontIntValue = snapshot.value[@"frontOfQueue"];


        callback(frontIntValue);
    }];


}


@end
