//
// Created by Joshua Taylor on 4/1/17.
// Copyright (c) 2017 Joshua Taylor. All rights reserved.
//

#import "Professor.h"
#import "Location.h"


@implementation Professor {



}

- (id)initWithSnapShot:(FIRDataSnapshot *)snapShot {

    if (self = [super init]) {

        self.firstName = snapShot.value[@"firstName"];
        self.lastName = snapShot.value[@"lastName"];
        self.roomNumber = snapShot.value[@"roomNumber"];
        self.roomCoordinate = [[Location alloc] initWithCoordinate:snapShot.value[@"roomCoordinate"]];
        self.imageLink = snapShot.value[@"imageLink"];

    }


    return self;
}


- (NSString *) getProfessorFullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

@end