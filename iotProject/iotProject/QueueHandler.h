//
//  QueueHandler.h
//  iotProject
//
//  Created by Joshua Taylor on 4/9/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QueuePacket;

@interface QueueHandler : NSObject

@property (strong, nonatomic) NSString *frontQueuePosition;
@property (strong, nonatomic) NSNumber *isRequestCompleted;

+ (QueueHandler *) sharedInstance;


- (void) cancelRequest;

-  (void) sendRequest:(QueuePacket *)questPacket;

- (BOOL) isCurrentRequest;




@end
