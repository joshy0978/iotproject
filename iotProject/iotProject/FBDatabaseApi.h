//
//  FBDatabaseApi.h
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Robot;
@class QueuePacket;
@class QueueHandler;

@interface FBDatabaseApi : NSObject

+ (FBDatabaseApi*)sharedInstance;


- (void)getProfessorDataWithBlock:(void (^)(NSArray * data))callback;

- (void)getRobotStatusWithBlock:(void (^)(Robot * robot)) callback;

- (void)pushQueuePacket:(QueuePacket *)queuePacket withBlock:(void (^)(NSString *queuePosition))callback;


- (void)cancelRequest:(NSString *) queuePosition;

- (void)getFrontOfQueueValue:(void (^)(NSString * queueSize)) callback;



@end
