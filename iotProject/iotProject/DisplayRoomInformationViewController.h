//
//  DisplayRoomInformationViewController.h
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Professor;

@interface DisplayRoomInformationViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) Professor *professor;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *roomNumber;
@property (weak, nonatomic) IBOutlet UIPickerView *pickUpPointPicker;
@property (weak, nonatomic) IBOutlet UILabel *pickupLabel;

@property (weak, nonatomic) IBOutlet UIButton *goButton;
@end
