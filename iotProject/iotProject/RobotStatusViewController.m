//
//  RobotStatusViewController.m
//  iotProject
//
//  Created by Joshua Taylor on 4/1/17.
//  Copyright © 2017 Joshua Taylor. All rights reserved.
//

#import "RobotStatusViewController.h"
#import "FBDatabaseApi.h"
#import "Robot.h"
#import "QueueHandler.h"


@interface RobotStatusViewController ()

@property  Boolean isFirstUse;
@property  (strong, nonatomic) Location* previousLocation;

@end

@implementation RobotStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isFirstUse = YES;


    self.closeWindowButton.alpha = 0;

    [[QueueHandler sharedInstance] sendRequest:self.queueItem];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];



    [[FBDatabaseApi sharedInstance] getRobotStatusWithBlock:^(Robot *robot) {


        [self changeRobotGoingToLabel:[NSString stringWithFormat:@"%@", robot.goingTo]];
        [self changeRobotStatus:[NSString stringWithFormat:@"%@", [robot getRobotStatus]]];
    }];
}

- (void) changeRobotStatus:(NSString *)text {






        if (!self.isFirstUse && ![text isEqualToString:self.statusLabel.text]) {

            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.statusLabel.alpha = 0;
                             }
                             completion:^(BOOL finished) {


                                        self.statusLabel.text = text;
                                       [self isRequestComplete];


                                 [UIView animateWithDuration:0.5 animations:^{
                                     self.statusLabel.alpha = 1;
                                 }];
                             }];

        } else if (self.isFirstUse) {

                self.statusLabel.text = text;

        }







}


- (void) changeRobotGoingToLabel:(NSString *) text {

    if (!self.isFirstUse && ![text isEqualToString:self.goToLabel.text]) {

        [UIView animateWithDuration:0.5
                         animations:^{
                             self.goToLabel.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             self.goToLabel.text = text;
                             [UIView animateWithDuration:0.5 animations:^{
                                 self.goToLabel.alpha = 1;
                             }];
                         }];

    } else if (self.isFirstUse) {

        self.goToLabel.text = text;
    }

    
    self.isFirstUse = NO;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelRequest:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Cancel Request?"
                                 message:@"If you cancel your request you will be removed from the queue. Are you sure?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self dismissViewControllerAnimated:YES completion:^{
                                        [[QueueHandler sharedInstance] cancelRequest];
                                    }];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)closeButtonAction:(id)sender {

    [self dismissViewControllerAnimated:YES completion:^{

    }];


    
}

- (void) isRequestComplete {
    
    NSLog(@"isRequestCompleted %i", [[QueueHandler sharedInstance].isRequestCompleted intValue]);
    NSString *boolValue = [[QueueHandler sharedInstance].isRequestCompleted stringValue];
    

    if (![boolValue isEqualToString:@"0"]) {

        [self changeButtonAnimation];

    }

}


- (void) changeButtonAnimation {

    [UIView animateWithDuration:0.5 animations:^{
        self.cancelRequestButton.alpha = 0;
        self.cancelRequestButton.enabled = NO;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            self.closeWindowButton.alpha = 1;
            self.closeWindowButton.enabled = YES;
        }];
    }];


}






@end
